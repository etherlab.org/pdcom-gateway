# PdCom Gateway

This is a tiny python application to implement protocol adaptors.

## Installation

You'll need [PdCom5](https://gitlab.com/etherlab.org/pdcom5) installed on your system.
If you're using OpenSuSe or Debian, have a look at the packages provided in
[OBS](https://build.opensuse.org/package/show/science:EtherLab/PdCom5).
Remeber, if you're installing PdCom5 from source,
you'll have to run `pip3 install .` in the `python` subdirectory of the PdCom5 sources.

To install this package, you can call `pip3 install .` from this source directory.
There is also a PyPi package repository available.
Therefore, you can run
`pip3 install PdCom-Gateway --extra-index-url https://gitlab.com/api/v4/projects/etherlab.org%2Fpdcom-gateway/packages/pypi/simple`
to install this package without having to download the sources.
